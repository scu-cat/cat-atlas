import Koa from 'koa';

const app = new Koa();
const PORT = process.env.PORT || 9001;

app.use(async ctx => {
  ctx.type = 'json';
  ctx.body = {
    name: 'xiaochuan',
    age: 21
  };
});

app.listen(PORT, () => {
  console.log(`server in port ${PORT}`);
});
