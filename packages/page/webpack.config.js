const { join } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const rootPath = __dirname;

/**
 * @type {import('webpack').Configuration}
 */
const config = {
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  entry: {
    main: join(rootPath, 'src', 'index.tsx'),
  },
  output: {
    filename: 'static/[name].js',
    chunkFilename: 'static/[name].js',
    path: join(rootPath, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.tsx$/,
        loader: 'ts-loader',
        options: {
          compilerOptions: {
            sourceMap: true
          }
        }
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
    ],
  },
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    'react-router-dom': 'ReactRouterDOM'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.json']
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          reuseExistingChunk: true,
          name: 'runtime',
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: join(rootPath, 'public', 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: 'static/[name].css',
    }),
  ],
  devServer: {
    static: {
      directory: join(rootPath, 'dist'),
    },
    compress: true,
    port: 9000,
  },
  devtool: process.env.NODE_ENV === 'production' ? false: 'source-map'
};

module.exports = config;
