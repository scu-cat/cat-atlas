import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './view/home';
import About from './view/about';

function App() {
  return (
    <Router basename="/">
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
