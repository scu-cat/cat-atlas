import React from 'react';
import { useHistory, Link } from 'react-router-dom';

function Home() {
  const history = useHistory();

  return <div onClick={() => history.push('/about')}>home<Link to="/about">to about</Link></div>;
}

export default Home;
